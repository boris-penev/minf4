#!/bin/sh

. /usr/share/java-utils/java-functions
MAIN_CLASS="weka.gui.GUIChooser"
set_classpath "weka.jar"
set_classpath "libsvm.jar"
set_classpath "java_cup.jar"
set_classpath "postgresql-jdbc"
set_classpath "hsqldb"
set_classpath "mysql-connector-java"
export CLASSPATH="$CLASSPATH:/usr/lib64/javasqlite/sqlite.jar"

mkdir -v tmp-weka

shopt -s extglob

for f in !(*-quadratic).arff
do
  basename="${f%%.arff}"
  linear_name="$basename"-classifier-output-linear-regression.txt
  m5p_name="$basename"-classifier-output-m5p-4.txt
  linear_availability_name="$basename"-classifier-output-availability-linear-regression.txt
  m5p_availability_name="$basename"-classifier-output-availability-m5p-4.txt
  java weka.classifiers.functions.LinearRegression -t "$f" \
    -split-percentage 70 -c 4 \
    -S 0 -R 1.0E-8 \
    > tmp-weka/"$linear_name"
  java weka.classifiers.trees.M5P -t "$f" \
    -split-percentage 70 -c 4 \
    -M 4.0 \
    > tmp-weka/"$m5p_name"
  if [ ! -f "$linear_name" ]
  then
    mv -v --no-clobber tmp-weka/"$linear_name" "$linear_name"
  else
    rm -v tmp-weka/"$linear_name"
  fi
  if [ ! -f "$m5p_name" ]
  then
    mv -v --no-clobber tmp-weka/"$m5p_name" "$m5p_name"
  else
    rm -v tmp-weka/"$m5p_name"
  fi
  #if [[ ! "$basename" == *-cpus ]]
  if [ -n "${basename%%*-cpus}" ]
  then
    java weka.classifiers.meta.FilteredClassifier \
      -t "$f" -split-percentage 70 -c last \
      -F "weka.filters.unsupervised.attribute.Remove -R 1,3" \
      -W weka.classifiers.functions.LinearRegression -- -S 0 -R 1.0E-8 \
      > tmp-weka/"$linear_availability_name"
    java weka.classifiers.meta.FilteredClassifier \
      -t "$f" -split-percentage 70 -c last \
      -F "weka.filters.unsupervised.attribute.Remove -R 1,3" \
      -W weka.classifiers.trees.M5P -- -M 4.0 \
      > tmp-weka/"$m5p_availability_name"
    if [ ! -f "$linear_availability_name" ]
    then
      mv -v --no-clobber tmp-weka/"$linear_availability_name" "$linear_availability_name"
    else
      rm -v tmp-weka/"$linear_availability_name"
    fi
    if [ ! -f "$m5p_availability_name" ]
    then
      mv -v --no-clobber tmp-weka/"$m5p_availability_name" "$m5p_availability_name"
    else
      rm -v tmp-weka/"$m5p_availability_name"
    fi
  fi
done

shopt -u extglob

rmdir -v tmp-weka
