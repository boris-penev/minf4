#!/bin/sh

. /usr/share/java-utils/java-functions
MAIN_CLASS="weka.gui.GUIChooser"
set_classpath "weka.jar"
set_classpath "libsvm.jar"
set_classpath "java_cup.jar"
set_classpath "postgresql-jdbc"
set_classpath "hsqldb"
set_classpath "mysql-connector-java"
export CLASSPATH="$CLASSPATH:/usr/lib64/javasqlite/sqlite.jar"

mkdir tmp-weka

for f in *.csv
do
  echo 'cores,availability,memory,execution time' | cat - "$f" > tmp-weka/"$f"
  java weka.core.converters.CSVLoader tmp-weka/"$f" > "${f%%.csv}".arff
  rm tmp-weka/"$f"
done

rmdir tmp-weka
