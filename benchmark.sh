#!/bin/sh

# add the mount step to fstab
# also mount write-only directory
# also read a file to know what to execute

mkdir /tmp/host
mount -t 9p -o trans=virtio,version=9p2000.L /mnt/host /tmp/host

echo '4 4 1 7' > /proc/sys/kernel/printk

time /tmp/host/mem-limit 2> /tmp/result
