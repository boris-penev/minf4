#!/usr/bin/env python3

import random
import cgroupset

def gaussian_truncated(mean, deviation, left_limit, right_limit):
    while True:
      result = round(random.gauss(mean, deviation))
      if left_limit <= result <= right_limit:
        break
    return result

def cgroupset_random(multithreading=False):

    # total memory size on my machine is 2933428224 bytes
    availability = gaussian_truncated(50, 20, 1, 100)
    cores        = gaussian_truncated(2, 1.5, 1, 4)
    memory       = gaussian_truncated(1000 ** 3, 0.4 * (1000 ** 3), 1000,
                                      2933428224)
    # When running on multiple cores, availability must increase accordingly
    # otherwise it is divided between them.
    # If the benchmark uses only one car, increasing CPU availability is
    # just increasing the single core availability, and we do not want this
    # as it will skew the results - a process running on one core with the
    # same CPU availability should have the same execution time no matter how
    # many cores it is given.
    availability_cgroup = cores * availability if multithreading \
                          else availability
    # cores must be a range of CPUs
    cores_cgroup = '0' if cores == 1 else '0-' + str(cores - 1)

    cgroupset.cgroupset(cores_cgroup, availability_cgroup, memory)
    return (str(cores), availability, memory)

if __name__ == '__main__':
    cgroupset_random()
