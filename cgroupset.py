#!/usr/bin/env python3

import socket
import subprocess
import check_return_value

def cgroupset(cores: str, availability: int, memory: int) -> int:
    print ('Availability:\t' + str(availability))
    print ('Cores:\t\t' + cores)
    print ('Memory:\t\t' + str(memory))

    _, host = socket.gethostname().split('.', 1)
    cgroup_name = 's1249355_minf4' if host == 'inf.ed.ac.uk' else 'minf4'

    cgcommand = []

    if cores != '':
        cgcommand.extend(['-r', 'cpuset.cpus=' + cores, '-r', 'cpuset.mems=0'])

    if availability != '':
        # cfs period is 100,000
        # availability is a percentage
        # (availability / 100) * 100,000 = availability * 1000
        cgcommand.extend(['-r', 'cpu.cfs_quota_us=' + str(availability * 1000)])

    # total memory size on my machine is 2933428224 bytes
    # total swap space on my machine is 2969563136 bytes
    # total memory+swap space on my machine is 5902991360 bytes
    # We do not limit swap space because this causes the operating system
    # to kill the process when running out of swap space.
    if memory != '':
        cgcommand.extend(['-r', 'memory.limit_in_bytes=' + str(memory),
                          '-r', 'memory.memsw.limit_in_bytes=5902991360'])

    if cgcommand != '':
        cgcommand.insert(0,'cgset')
        cgcommand.append(cgroup_name)
        return_value = subprocess.call(cgcommand)
        check_return_value.check_return_value(cgcommand, return_value)
