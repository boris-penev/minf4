#!/usr/bin/env python3

import argparse
import csv
import matplotlib.pyplot
import numpy
import os

def pearson(cores, avail, memory, time, f):
    corrcoef_cores = numpy.corrcoef(cores, time)
    corrcoef_avail = numpy.corrcoef(avail, time)
    corrcoef_memory = numpy.corrcoef(memory, time)

    print('Pearson coefficient')
    print('Cores:\t\t', corrcoef_cores[0][1])
    print('Availability:\t', corrcoef_avail[0][1])
    print('Memory:\t\t', corrcoef_memory[0][1])

    print('Pearson', file=f)
    print('Cores:\t\t', corrcoef_cores[0][1], file=f)
    print('Availability:\t', corrcoef_avail[0][1], file=f)
    print('Memory:\t\t', corrcoef_memory[0][1], file=f)

def importance_index(cores, avail, memory, time, f):
    v_cores = numpy.var(cores, ddof=1)
    v_avail = numpy.var(avail, ddof=1)
    v_memory = numpy.var(memory, ddof=1)
    v_time = numpy.var(time, ddof=1)
    i_cores = v_cores / v_time
    i_avail = v_avail / v_time
    i_memory = v_memory / v_time

    print('Importance Index')
    print('Cores:\t\t', i_cores)
    print('Availability:\t', i_avail)
    print('Memory:\t\t', i_memory)

    print('Importance Index', file=f)
    print('Cores:\t\t', i_cores, file=f)
    print('Availability:\t', i_avail, file=f)
    print('Memory:\t\t', i_memory, file=f)

def polynomial_regression(cores, avail, memory, time, f, save, benchmark_name):
    degree = 2
    r_cores = numpy.polyfit(cores, time, degree)
    r_avail = numpy.polyfit(avail, time, degree)
    r_memory = numpy.polyfit(memory, time, degree)

    print('Polynomial regression')
    print('Cores:\t\t', r_cores)
    print('Availability:\t', r_avail)
    print('Memory:\t\t', r_memory)

    print('Polynomial regression', file=f)
    print('Cores:\t\t', r_cores, file=f)
    print('Availability:\t', r_avail, file=f)
    print('Memory:\t\t', r_memory, file=f)

    # cores
    x = numpy.linspace(1, 4, 200)
    y = 0
    for i in range(degree + 1):
        y += r_cores[i] * x ** (degree - i)
    fig = matplotlib.pyplot.figure()
    fig.suptitle(benchmark_name)
    matplotlib.pyplot.title('Cores Polynomial Regression')
    matplotlib.pyplot.plot(x, y, color='black')
    if save:
        matplotlib.pyplot.savefig(benchmark_name + '_cores_regression')
    else:
        matplotlib.pyplot.show()
    matplotlib.pyplot.close(fig)

    # availability
    x = numpy.linspace(1, 100, 200)
    y = 0
    for i in range(degree + 1):
        y += r_avail[i] * x ** (degree - i)
    fig = matplotlib.pyplot.figure()
    fig.suptitle(benchmark_name)
    matplotlib.pyplot.title('Availability Polynomial Regression')
    matplotlib.pyplot.plot(x, y, color='black')
    if save:
        matplotlib.pyplot.savefig(benchmark_name + '_availability_regression')
    else:
        matplotlib.pyplot.show()
    matplotlib.pyplot.close(fig)


    # memory
    x = numpy.linspace(1, 2900 * 1000 * 1000, 200)
    y = 0
    for i in range(degree + 1):
        y += r_memory[i] * x ** (degree - i)
    fig = matplotlib.pyplot.figure()
    fig.suptitle(benchmark_name)
    matplotlib.pyplot.title('Memory Polynomial Regression')
    matplotlib.pyplot.plot(x, y, color='black')
    if save:
        matplotlib.pyplot.savefig(benchmark_name + '_memory_regression')
    else:
        matplotlib.pyplot.show()
    matplotlib.pyplot.close(fig)

# TODO this can be a filter lambda expression, right?
def clean_outliers(x, y, median, factor):
    x_cleaned = []
    y_cleaned = []

    for i in range(len(y)):
        if y[i] - median < factor * median:
            x_cleaned.append(int(x[i]))
            y_cleaned.append(float(y[i]))

    return (x_cleaned, y_cleaned)

def scatter_plot(cores, avail, memory, time, f, save, benchmark_name):
    mean_time = numpy.mean(time)
    std_time = numpy.std(time, ddof=2)
    median_time = numpy.median(time)

    print('Mean:\t\t\t', mean_time)
    print('Median:\t\t\t', median_time)
    print('Standard deviation:\t', std_time)

    print('Mean:\t\t\t', mean_time, file=f)
    print('Median:\t\t\t', median_time, file=f)
    print('Standard deviation:\t', std_time, file=f)

    #for i in range(len(time)):
        #if time[i] - median_time < 10 * median_time:
            #cores_cleaned.append(int(cores[i]))
            #avail_cleaned.append(int(avail[i]))
            #memory_cleaned.append(int(memory[i]))
            #time_cleaned.append(float(time[i]))
        #else:
            #print(benchmark_name + ' outlier t ' + str(time[i])
                  #+ ' m ' + str(median_time) + ' a ' + str(avail[i])
                  #+ ' c ' + str(cores[i]) + ' m ' + str(memory[i]/1000**2))

    cmap = matplotlib.pyplot.get_cmap('gray')
    short_name = benchmark_name.split('spec-cpu2006-', 1)[1] \
        if benchmark_name.startswith('spec-cpu2006-') \
        else benchmark_name

    # cores
    (cores_cleaned, time_cleaned) = clean_outliers(cores, time, median_time, 2)
    fig = matplotlib.pyplot.figure()
    # fig.suptitle(benchmark_name)
    matplotlib.rcParams.update({'font.size': 25})
    matplotlib.pyplot.gcf().subplots_adjust(left=0.20, bottom=0.15)
    matplotlib.pyplot.xlabel('#cores', size=25)
    matplotlib.pyplot.ylabel('Exec. time (secs)', size=25)
    matplotlib.pyplot.title(short_name + ' Cores', fontsize=30)
    matplotlib.pyplot.grid(True)
    axes = matplotlib.pyplot.gca()
    # EEMBC benchmarks have outliers skewing plots
    #if benchmark_name == short_name and benchmark_name != 'build_kernel':
        #axes.set_ylim(top=150)
    matplotlib.pyplot.scatter(cores_cleaned, time_cleaned, s=256, cmap=cmap,
        color='black', marker='x')
    axes.set_xbound(lower=0.9, upper=4.1)
    axes.set_ybound(lower=0)
    if save:
        matplotlib.pyplot.savefig(benchmark_name + '_cores_scatter')
    else:
        matplotlib.pyplot.show()
    matplotlib.pyplot.close(fig)

    # availability
    (avail_cleaned, time_cleaned) = clean_outliers(avail, time, median_time, 10)
    fig = matplotlib.pyplot.figure()
    # fig.suptitle(benchmark_name)
    matplotlib.rcParams.update({'font.size': 25})
    matplotlib.pyplot.gcf().subplots_adjust(left=0.20, bottom=0.15)
    matplotlib.pyplot.xlabel('CPU avail. (%)', size=25)
    matplotlib.pyplot.ylabel('Exec. time (secs)', size=25)
    matplotlib.pyplot.title(short_name + ' Availability',
                            fontsize=30)
    matplotlib.pyplot.grid(True)
    axes = matplotlib.pyplot.gca()
    #if benchmark_name == short_name and benchmark_name != 'build_kernel':
        #axes.set_ylim(top=150)
    matplotlib.pyplot.scatter(avail_cleaned, time_cleaned, s=256, cmap=cmap,
        color='black', marker='x')
    # EEMBC benchmarks have lower availability limit
    if benchmark_name == short_name and benchmark_name != 'build_kernel':
        axes.set_xbound(lower=4, upper=105)
    else:
        axes.set_xbound(lower=18, upper=102)
    axes.set_ybound(lower=0)
    if save:
        matplotlib.pyplot.savefig(benchmark_name + '_availability_scatter')
    else:
        matplotlib.pyplot.show()
    matplotlib.pyplot.close(fig)


    # memory
    (memory_cleaned, time_cleaned) = clean_outliers(memory, time, median_time, 10)
    fig = matplotlib.pyplot.figure()
    # fig.suptitle(benchmark_name)
    matplotlib.rcParams.update({'font.size': 25})
    matplotlib.pyplot.gcf().subplots_adjust(left=0.20, bottom=0.15)
    matplotlib.pyplot.xlabel('Memory avail. (bytes)', size=25)
    matplotlib.pyplot.ylabel('Exec. time (secs)', size=25)
    matplotlib.pyplot.title(short_name + ' Memory',
                            fontsize=30)
    matplotlib.pyplot.grid(True)
    axes = matplotlib.pyplot.gca()
    #if benchmark_name == short_name and benchmark_name != 'build_kernel':
        #axes.set_ylim(top=150)
    matplotlib.pyplot.scatter(memory_cleaned, time_cleaned, s=256, cmap=cmap,
        color='black', marker='x')
    axes.set_ybound(lower=0)
    if save:
        matplotlib.pyplot.savefig(benchmark_name + '_memory_scatter')
    else:
        matplotlib.pyplot.show()
    matplotlib.pyplot.close(fig)

def scatter_plot_raw(cores, avail, memory, time, save, benchmark_name):

    cmap = matplotlib.pyplot.get_cmap('gray')
    short_name = benchmark_name.split('spec-cpu2006-', 1)[1] \
        if benchmark_name.startswith('spec-cpu2006-') \
        else benchmark_name

    # cores
    fig = matplotlib.pyplot.figure()
    # fig.suptitle(benchmark_name)
    matplotlib.rcParams.update({'font.size': 25})
    matplotlib.pyplot.gcf().subplots_adjust(left=0.20, bottom=0.15)
    matplotlib.pyplot.xlabel('#cores', size=25)
    matplotlib.pyplot.ylabel('Exec. time (secs)', size=25)
    matplotlib.pyplot.title(short_name + ' Cores', fontsize=30)
    matplotlib.pyplot.grid(True)
    matplotlib.pyplot.scatter(cores, time, s=256, cmap=cmap,
        color='black', marker='x')
    axes = matplotlib.pyplot.gca()
    axes.set_xbound(lower=0.9, upper=4.1)
    axes.set_ybound(lower=0)
    if save:
        matplotlib.pyplot.savefig(benchmark_name + '_cores_scatter_raw')
    else:
        matplotlib.pyplot.show()
    matplotlib.pyplot.close(fig)

    # availability
    fig = matplotlib.pyplot.figure()
    # fig.suptitle(benchmark_name)
    matplotlib.rcParams.update({'font.size': 25})
    matplotlib.pyplot.gcf().subplots_adjust(left=0.20, bottom=0.15)
    matplotlib.pyplot.xlabel('CPU avail. (%)', size=25)
    matplotlib.pyplot.ylabel('Exec. time (secs)', size=25)
    matplotlib.pyplot.title(short_name + ' Availability',
                            fontsize=30)
    matplotlib.pyplot.grid(True)
    matplotlib.pyplot.scatter(avail, time, s=256, cmap=cmap,
        color='black', marker='x')
    axes = matplotlib.pyplot.gca()
    # EEMBC benchmarks have lower availability limit
    if benchmark_name == short_name and benchmark_name != 'build_kernel':
        axes.set_xbound(lower=-1, upper=102)
    else:
        axes.set_xbound(lower=18, upper=102)
    axes.set_ybound(lower=0)
    if save:
        matplotlib.pyplot.savefig(benchmark_name + '_availability_scatter_raw')
    else:
        matplotlib.pyplot.show()
    matplotlib.pyplot.close(fig)


    # memory
    fig = matplotlib.pyplot.figure()
    # fig.suptitle(benchmark_name)
    matplotlib.rcParams.update({'font.size': 25})
    matplotlib.pyplot.gcf().subplots_adjust(left=0.20, bottom=0.15)
    matplotlib.pyplot.xlabel('Memory avail. (bytes)', size=25)
    matplotlib.pyplot.ylabel('Exec. time (secs)', size=25)
    matplotlib.pyplot.title(short_name + ' Memory',
                            fontsize=30)
    matplotlib.pyplot.grid(True)
    matplotlib.pyplot.scatter(memory, time, s=256, cmap=cmap,
        color='black', marker='x')
    axes = matplotlib.pyplot.gca()
    axes.set_xbound(lower=-20)
    axes.set_ybound(lower=0)
    if save:
        matplotlib.pyplot.savefig(benchmark_name + '_memory_scatter_raw')
    else:
        matplotlib.pyplot.show()
    matplotlib.pyplot.close(fig)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--save',
                        action='store_true',
                        dest = 'save',
                        required = False,
                        default = False,
                        help = 'Save the images as files')
    parser.add_argument(dest = 'filenames',
                        type = str,
                        nargs = '+',
                        help = 'Benchmark results to evaluate')

    args = parser.parse_args()

    assert len(args.filenames) > 0
    filenames = args.filenames
    save = args.save is True

    for filename in filenames:
        cores = []
        avail = []
        memory = []
        time = []
        with open(filename, 'r') as f:
            reader = csv.reader(f)
            for line in reader:
                cores.append(int(line[0]))
                avail.append(int(line[1]))
                memory.append(int(line[2]))
                time.append(float(line[3]))
        output_filename = filename.rsplit(sep='.csv', maxsplit=1)[0]
        benchmark_name = os.path.basename(output_filename)
        output_filename = output_filename + '-eval.txt'
        print(benchmark_name)
        with open(output_filename, 'w') as f:
            pearson(cores, avail, memory, time, f)
            importance_index(cores, avail, memory, time, f)
            scatter_plot(cores, avail, memory, time, f, save, benchmark_name)
            # polynomial_regression(cores, avail, memory, time, f, save,
            #                       benchmark_name)


if __name__ == '__main__':
    main()
