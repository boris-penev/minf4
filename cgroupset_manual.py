#!/usr/bin/env python3

import argparse
import cgroupset

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-a', '--availability',
                        type = int,
                        dest = 'availability',
                        default = '',
                        required = False,
                        help = 'CPU availability')
    parser.add_argument('-c', '--cores',
                        type = str,
                        dest = 'cores',
                        default = '',
                        required = False,
                        help = 'CPU cores')
    parser.add_argument('-m', '--memory',
                        type = int,
                        dest = 'memory',
                        default = '',
                        required = False,
                        help = 'Memory')

    args = parser.parse_args()

    availability = args.availability
    cores        = args.cores
    memory       = args.memory

    cgroupset.cgroupset(cores, availability, memory)

if __name__ == '__main__':
    main()
