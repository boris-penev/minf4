#!/usr/bin/env python3

import argparse
import socket
import subprocess
import time
import cgroupset_random
import check_return_value

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-b', '--benchmark',
                        type = str,
                        dest = 'benchmark',
                        required = True,
                        help = 'Benchmark to run')
    parser.add_argument('-i', '--iterations',
                        type = int,
                        dest = 'iterations',
                        required = False,
                        default = 1,
                        help = 'Number of times to run the benchmark')
    parser.add_argument('-o', '--output-file',
                        type = str,
                        dest = 'output_filename',
                        required = False,
                        help = 'Output file for the results, for file sake')

    args = parser.parse_args()

    benchmark       = args.benchmark.split()
    iterations      = args.iterations
    output_filename = args.output_filename

    for i in range(iterations):
        run_benchmark(benchmark, output_filename)

def run_benchmark(benchmark, output_filename=None):
    (cores, availability, memory) = cgroupset_random.cgroupset_random()

    # memory controller is not created on university computers
    _, host = socket.gethostname().split('.', 1)
    if host == 'inf.ed.ac.uk':
        cgroup_name = 's1249355_minf4'
    else:
        cgroup_name = 'minf4'
    memory_controller = ',memory'

    benchmark_command = ['cgexec', '-g',
                         'cpuset,cpu' + memory_controller + ':' + cgroup_name]
    benchmark_command.extend(benchmark)

    time_begin = time.perf_counter()
    return_value = subprocess.call(benchmark_command)
    elapsed_time = time.perf_counter() - time_begin
    check_return_value.check_return_value(benchmark_command, return_value)
    print('Elapsed time:\t' + str(elapsed_time))
    if output_filename is not None:
        with open(output_filename, 'a') as output_file:
            print(cores + ',' + str(availability) + ',' + str(memory) + ',' +
                  str(round(elapsed_time, 3)), file=output_file)

if __name__ == '__main__':
    main()
