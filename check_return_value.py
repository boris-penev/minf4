#!/usr/bin/env python3

def check_return_value(command: str, return_value: int) -> None:
    if return_value != 0:
        raise RuntimeError('Return code of command is non-zero\n' +
                            'Command: ' + ' '.join(command) + '\n' +
                            'Return code: ' + str(return_value))
